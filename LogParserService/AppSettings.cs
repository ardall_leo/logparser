﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogParserService
{
    public class AppSettings
    {
        public string SourcePath { get; set; }
        public string OutputPath { get; set; }
        public bool StoreToDb { get; set; }
        public string DatabaseServer { get; set; }
        public string DatabaseName { get; set; }
    }
}
