using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LogParserService.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace LogParserService
{
    public class Program
    {
        static ILogger logger;
        static DateTime startTime;
        public static async Task Main(string[] args)
        {
            var baseDir = AppDomain.CurrentDomain.BaseDirectory;
            startTime = DateTime.Now;
            Log.Logger = new LoggerConfiguration()
            .WriteTo.Console()
            .CreateLogger();

            Log.Information("Intializing...");

            try
            {
                var isService = !(Debugger.IsAttached || args.Contains("--console"));

                Log.Information("====================================================================");
                Log.Information($"Application Starts. Version: {System.Reflection.Assembly.GetEntryAssembly()?.GetName().Version}");
                Log.Information($"Application Directory: {baseDir}");
                var builder = BuildService(); // CreateHostBuilder(args);

                if (isService)
                {
                    await builder
                        .UseWindowsService()
                        .Build()
                        .RunAsync();
                }
                else
                {
                    await builder.RunConsoleAsync();
                }
            }
            catch (Exception e)
            {
                Log.Fatal(e, "Application terminated unexpectedly");
            }
            finally
            {
                Log.Information("====================================================================\r\n");
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddScoped<ILogParser, Parser>();
                    services.Configure<AppSettings>(hostContext.Configuration.GetSection("AppSettings"));
                    //services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));

                    services.AddLogging(configure => configure.AddSerilog(Log.Logger));
                    services.AddHostedService<Worker>();
                })
            .UseWindowsService();

        private static IHostBuilder BuildService()
        {
            var bootstrapConfigRoot = new ConfigurationBuilder()
               .AddJsonFile($"appsettings.json")
               .Build();

            var builder = new HostBuilder()
                .ConfigureServices((builderContext, services) =>
                {
                    services.AddScoped<ILogParser, Parser>();
                    services.Configure<AppSettings>(options => bootstrapConfigRoot.GetSection("AppSettings").Bind(options));

                    services.AddHostedService<Worker>();
                    services.AddLogging(x => x.AddSerilog(Log.Logger));
                });

            return builder;
        }

    }
}
