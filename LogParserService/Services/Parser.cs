﻿using LogParser.Common.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Serilog;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using LogParser.Common.Report;
using CsvHelper;
using LogParser.Common.Storage;
using System.Threading;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Globalization;
using LogParser.Common.Map;

namespace LogParserService.Services
{
    public interface ILogParser
    {
        void StartParsing(string filePath, bool checkAll = false);
    }
    public class Parser: ILogParser
    {
        private readonly ILogger<Parser> _logger;
        private readonly string _sourcePath;
        private readonly string _outputPath;
        private readonly bool _isStoreToDB;

        public Parser(ILogger<Parser> logger, IOptions<AppSettings> settings)
        {
            _logger = logger;
            _sourcePath = settings.Value.SourcePath;
            _outputPath = settings.Value.OutputPath;
            _isStoreToDB = settings.Value.StoreToDb;
        }
        public void StartParsing(string filePath, bool checkAll = false)
        {
            
            if (checkAll)
            {
                var fileConfigPath = Directory.EnumerateFiles(_sourcePath);

                _logger.LogInformation("Locating log files..Please wait...");

                Parallel.ForEach(fileConfigPath, ProcessFile);
            }
            else
            {
                ProcessFile(filePath);
            }
        }

        private void ProcessFile(string filePath)
        {
            try
            {
                List<LogItem> logItems = LoadFile(filePath);
                if (logItems != null)
                {
                    Report report = new Report(logItems);

                    // RAW
                    _logger.LogInformation($"Converting RAW data from file {filePath}");
                    using (var writer = new StreamWriter($@"{_outputPath}\raw-{DateTime.Now.Ticks}.csv"))
                    using (var csv = new CsvWriter(writer))
                    {
                        csv.Configuration.RegisterClassMap<LogItemMap>();
                        csv.WriteRecords(logItems);
                    }

                    // Aggregat 1
                    _logger.LogInformation($"Create Call data from file {filePath}");
                    using (var writer = new StreamWriter($@"{_outputPath}\report-{DateTime.Now.Ticks}.csv"))
                    using (var csv = new CsvWriter(writer))
                    {
                        csv.Configuration.RegisterClassMap<CallMap>();
                        csv.WriteRecords(report.ListOfCall2);

                        if (this._isStoreToDB)
                        {
                            foreach (var c in report.ListOfCall2)
                            {
                                DocumentStoreHolder.Save(c);
                            }
                        }
                    }

                    // Aggregat 2
                    _logger.LogInformation($"Create Call Stats data from file {filePath}");
                    using (var writer = new StreamWriter($@"{_outputPath}\report2-{DateTime.Now.Ticks}.csv"))
                    using (var csv = new CsvWriter(writer))
                    {
                        csv.Configuration.RegisterClassMap<CallStatsMap>();
                        csv.WriteRecords(report.GetStats);

                        if (this._isStoreToDB)
                        {
                            foreach (var c in report.GetStats)
                            {
                                DocumentStoreHolder.Save(c);
                            }
                        }
                    }

                    File.Move(filePath, filePath + ".bak");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error processing file: {filePath}", ex);
            }
        }

        private List<LogItem> LoadFile(string filePath)
        {
            var fileExt = Path.GetExtension(filePath);

            if (!fileExt.ToLower().EndsWith("txt"))
            {
                return null;
            }

            string[] readText = File.ReadAllLines(filePath);
            List<LogItem> logItems = new List<LogItem>();

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            for (int i = 0; i < (readText.Length - 2); i += 2)
            {
                try
                {
                    logItems.Add(new LogItem()
                    {
                        Waktu = DateTime.ParseExact(readText[i], "dd-MM-yyyy hh:mm:ss", CultureInfo.InvariantCulture),
                        Data = JsonConvert.DeserializeObject<LogData>(readText[i + 1], settings)
                    });

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return logItems;

            return null;
        }
    }
}
