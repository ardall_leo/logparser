param(
        [ValidateScript({
            if( -Not ($_ | Test-Path) ){
                New-Item "$($_)\Service" -type directory
				New-Item "$($_)\Web" -type directory
            }
            return $true
        })]
        [System.IO.FileInfo]$Path
)

$ServiceName = "LogParserService"
$WebAppName = "LogParserWeb"

function Clean-Up
{
	Write-Host "Stopping Service"
	if (Get-Service $ServiceName -ErrorAction SilentlyContinue) {
		Get-Service $ServiceName | Stop-Service
		
		WaitUntilServices $ServiceName "Stopped"
		Remove-Service $ServiceName -Verbose
	} 
	else {
		Write-Output "Window service - '$ServiceName' does not exist. Uninstallation Complete"
	}
	
	Get-Process -Name $WebAppName -ErrorAction SilentlyContinue | Stop-Process
	
	Remove-Item "$($Path)\" -Recurse
}

function WaitUntilServices($searchString, $status)
{
    # Get all services where DisplayName matches $searchString and loop through each of them.
    foreach($service in (Get-Service -DisplayName $searchString))
    {
        # Wait for the service to reach the $status or a maximum of 30 seconds
        $service.WaitForStatus($status, '00:00:30')
    }
}

function Install-Service
{
	Write-Host "Installing Service"
	Expand-Archive -Path .\Deploy\LogParserService.zip -DestinationPath "$($Path)\Service"

	Write-Host "Creating Service"
	New-Service -Name "LogParserService" -BinaryPathName "$Path\Service\LogParserService.exe"

	Write-Host "Starting Service"
	Start-Service -Name "LogParserService"
	Write-Host "Service Started." -ForegroundColor Green
}

function Install-Web
{
	Write-Host "Instaling Web"
	Expand-Archive -Path .\Deploy\LogParserWeb.zip -DestinationPath "$($Path)\Web"
	
	Start-Process -FilePath "$($Path)\Web\LogParserWeb.exe" -WorkingDirectory "$($Path)\Web"
	Start http://localhost:3000/#/dashboard
}

function Run-Registry
{
	Write-Host "Run Registry"
	regedit /s "Carano.reg"
}

Run-Registry

Write-Host "Cleaning up..."
Clean-Up
	
Install-Service
Install-Web