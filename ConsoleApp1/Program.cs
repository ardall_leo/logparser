﻿using CsvHelper;
using LogParser.Common;
using LogParser.Common.Model;
using LogParser.Common.Report;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Extensions.Configuration;
using System.Threading;
using LogParser.Common.Storage;
using Raven.Client.Documents.Session;
using Raven.Client.Documents;

namespace LogParser
{
    class Program
    {
        static ILogger logger;
        static DateTime startTime;
        static IConfigurationRoot configuration;
        static string fileConfigPath;
        static int logFileCount = 1;
        static void Main(string[] args)
        {
            startTime = DateTime.Now;
            logger = new LoggerConfiguration()
            .WriteTo.Console()
            .CreateLogger();

            logger.Information("Intializing...");

            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            configuration = builder.Build();

            StartParsing();
            var calls = DocumentStoreHolder.GetAll<CallStats>();

            Console.Write("Press enter to exit...");
            Console.ReadKey();
        }

        private static void StartParsing()
        {
            var fileConfigPath = Directory.EnumerateFiles(configuration.GetSection("sourcePath").Value);

            logger.Information("Locating log files..Please wait...");

            Parallel.ForEach(fileConfigPath, ProcessFile);

            if (logFileCount > 0)
            {
                logger.Information($"Done parsing {logFileCount} file(s) in {DateTime.Now.Subtract(startTime).TotalSeconds} seconds");
            }
            else
            {
                logger.Warning("Can not find any log files");
            }
        }

        private static void ProcessFile(string filePath)
        {
            try
            {
                List<LogItem> logItems = LoadFile(filePath);
                if (logItems != null)
                {
                    Interlocked.Increment(ref logFileCount);
                    //testing purpose
                    //logItems.ForEach(i => i.Waktu = i.Waktu.AddDays(logFileCount));

                    Report report = new Report(logItems);

                    // RAW
                    logger.Information($"Converting RAW data from file {filePath}");
                    using (var writer = new StreamWriter($@"{configuration.GetSection("outputPath").Value}\raw-{DateTime.Now.Ticks}.csv"))
                    using (var csv = new CsvWriter(writer))
                    {
                        csv.WriteRecords(logItems);
                    }

                    // Aggregat 1
                    logger.Information($"Create Call data from file {filePath}");
                    using (var writer = new StreamWriter($@"{configuration.GetSection("outputPath").Value}\report-{DateTime.Now.Ticks}.csv"))
                    using (var csv = new CsvWriter(writer))
                    {
                        csv.Configuration.RegisterClassMap<CallMap>();
                        csv.WriteRecords(report.ListOfCall2);

                        foreach(var c in report.ListOfCall2)
                        {
                            DocumentStoreHolder.Save(c);
                        }
                    }

                    // Aggregat 2
                    logger.Information($"Create Call Stats data from file {filePath}");
                    using (var writer = new StreamWriter($@"{configuration.GetSection("outputPath").Value}\report2-{DateTime.Now.Ticks}.csv"))
                    using (var csv = new CsvWriter(writer))
                    {
                        csv.Configuration.RegisterClassMap<CallStatsMap>();
                        csv.WriteRecords(report.GetStats);

                        foreach (var c in report.GetStats)
                        {
                            DocumentStoreHolder.Save(c);
                        }
                    }

                    File.Move(filePath, filePath + ".bak");
                }
                //logger.Information(JsonConvert.SerializeObject(x));
            }
            catch (Exception ex)
            {
                logger.Error($"Error processing file: {filePath}", ex);
            }
        }

        private static List<LogItem> LoadFile(string filePath)
        {
            var fileExt = Path.GetExtension(filePath);

            if (!fileExt.ToLower().EndsWith("txt"))
            {
                return null;
            }

            string[] readText = File.ReadAllLines(filePath);
            List<LogItem> logItems = new List<LogItem>();

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Error;

            for (int i = 0; i < (readText.Length - 2); i += 2)
            {
                try
                {
                    logItems.Add(new LogItem()
                    {
                        Waktu = DateTime.ParseExact(readText[i], "dd-MM-yyyy hh:mm:ss", CultureInfo.InvariantCulture),
                        Data = JsonConvert.DeserializeObject<LogData>(readText[i + 1], settings)
                    });

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return logItems;
        }
    }
}
