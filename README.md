This repository contains 3 projects:

- LogParserService
- LogParserWeb
- LogParserCommon

*LogParserService* is a window service that responsible to monitor and watch any log files that added to a specified directory then parse and insert the data to dabatase.

*LogParserWeb* is a web application to see the report of the log data per device site id. This aplication have some features to filter log based on selected years or with custom date range.

# Dependencies
- RavenDB 4.2.4 (https://hibernatingrhinos.com/downloads/RavenDB%20for%20Windows%20x64/42034)
- .NET Core 3.0
- Nodejs 10.16.1

Make sure RavenDB is installed and configure properly.
Extract the package and run this command to run RavenDB Server
```
& .\run.ps1
```
Here is the complete documentation: https://ravendb.net/docs/article-page/4.2/csharp/start/getting-started#installation--setup

# Configuration
RavenDB configuration located at `\Server\settings.json`
```
{
  "DataDir": "RavenData",
  "License.Eula.Accepted": true,
  "Setup.Mode": "Unsecured",
  "Security.UnsecuredAccessAllowed": "PublicNetwork",
  "ServerUrl": "http://127.0.0.1:8181",
  "ServerUrl.Tcp": "tcp://127.0.0.1:38888"
}
```
Please make sure the value of the `ServerUrl` matching with `DatabaseServer` value in registry file `Carano.reg`

The web application by default will run at port `3000`.
If you want to change it, you can change it in `\{Web_Project_Path}\appsettings.json`
```
{
    "Logging": {
        "IncludeScopes": false,
        "LogLevel": {
            "Default": "Warning"
        }
    },
    "Port":  3000
}
```
# Development
```
## building the UI
cd LogParserWeb\ClientApp
npm install
npm start
```

# Build
```
& .\build.ps1
```

# Install
```
& .\install.ps1 -Path "{Installation_Folder}"
```