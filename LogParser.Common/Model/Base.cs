﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogParser.Common.Model
{
    public class BaseLog
    {
        public DateTime Date { get; set; }
        public int? DeviceSiteId { get; set; }
    }
}
