﻿using CsvHelper.Configuration.Attributes;
using Newtonsoft.Json;

namespace LogParser.Common.Model
{
    public class Source
    {
        [JsonProperty(PropertyName = "Redundancy Group")]
        [Name("Source_Redudancy_Group")]
        public int RedudancyGroup { get; set; }

        [JsonProperty(PropertyName = "Repeater Id")]
        [Name("Source_Repeater_Id")]
        public int RepeaterId { get; set; }

        [JsonProperty(PropertyName = "Device Site Id")]
        [Name("Source_Device_Id")]
        public int DeviceSiteId { get; set; }

        /// <summary>
        /// both_channels
        /// first_channel
        /// second_channel
        /// </summary>
        [JsonProperty(PropertyName = "Repeater Slots")]
        [Name("Source_Repeater_Slots")]
        public string RepeaterSlots { get; set; }

        [JsonProperty(PropertyName = "App Type Id")]
        [Name("Source_App_Type_Id")]
        public int AppTypeId { get; set; }

        [JsonProperty(PropertyName = "Server Id")]
        [Name("Source_Server_Id")]
        public int ServerId { get; set; }
    }
}
