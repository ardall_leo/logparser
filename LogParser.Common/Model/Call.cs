﻿namespace LogParser.Common.Model
{
    using CsvHelper.Configuration;
    using LogParser.Common.Enum;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;

    public class Call
    {
        [JsonProperty(PropertyName = "__CREATE_TS")]
        public DateTime __CREATE_TS { get; set; }

        [JsonProperty(PropertyName = "Time Start")]
        public DateTime TimeStart { get; set; }

        [JsonProperty(PropertyName = "Time End")]
        public DateTime TimeEnd { get; set; }

        [JsonProperty(PropertyName = "Time Duration")]
        public double Duration {
            get
            {
                return TimeEnd != default(DateTime) ? (TimeEnd - TimeStart).TotalSeconds : 0;
            }
        }

        [JsonProperty(PropertyName = "Site Id")]
        public int? DeviceSiteId { get; set; }

        [JsonProperty(PropertyName = "Repeater Id")]
        public int? RepeaterId { get; set; }

        [JsonProperty(PropertyName = "Repeater Slots")]
        public string RepeaterSlots { get; set; }

        [JsonProperty(PropertyName = "Source")]
        public int Source { get; set; }

        [JsonProperty(PropertyName = "Target")]
        public int Target { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = "Call Type")]
        public CallType CallType { get; set; }

        [JsonIgnore]
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = "Action")]
        public CallAction Action { get; set; }

        [JsonProperty(PropertyName = "Csn")]
        public int Csn { get; set; }

        [JsonIgnore]
        [JsonProperty(PropertyName = "Logical")]
        public int Logical { get; set; }

        [JsonProperty(PropertyName = "Call Status")]
        public CallStatus CallStatus { get; set; }

        [JsonProperty(PropertyName = "RSSI")]
        public string Rssi { get; set; }

        [JsonProperty(PropertyName = "Info")]
        public CallInfo Info { get; set; }
    }

    public sealed class CallMap : ClassMap<Call>
    {
        public CallMap()
        {
            AutoMap();
            Map(m => m.__CREATE_TS).Ignore();
            Map(m => m.Action).Ignore();
            Map(m => m.Logical).Ignore();
            Map(m => m.Info).Ignore();
        }
    }
}
