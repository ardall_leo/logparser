﻿using CsvHelper.Configuration;
using LogParser.Common.Enum;
using System;

namespace LogParser.Common.Model
{
    /// <summary>
    /// Call statistic per day
    /// </summary>
    public class CallStats: BaseLog
    {
        public string DeviceSiteName { get; set; }
        public CallType CallType { get; set; }
        public CallStatus CallStatus { get; set; }
        public int Total { get; set; }
    }


    public sealed class CallStatsMap : ClassMap<CallStats>
    {
        public CallStatsMap()
        {
            AutoMap();
        }
    }
}
