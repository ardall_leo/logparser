﻿using Newtonsoft.Json;

namespace LogParser.Common.Model
{
    public class ParticipatingSite
    {
        [JsonProperty(PropertyName = "Site")]
        public int Site { get; set; }

        [JsonProperty(PropertyName = "Site Type")]
        public string SiteType { get; set; }

        [JsonProperty(PropertyName = "Logical Channel Number")]
        public int LogicalChannelNumber { get; set; }
    }
}
