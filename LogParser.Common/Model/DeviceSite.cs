﻿using Newtonsoft.Json;

namespace LogParser.Common.Model
{
    public class DeviceSite
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
