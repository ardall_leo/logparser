﻿using CsvHelper.Configuration.Attributes;
using Newtonsoft.Json;

namespace LogParser.Common.Model
{

    public class Descriptor
    {
        [JsonProperty(PropertyName = "Protocol")]
        [Name("Descriptor_Protocol")]
        public int Protocol { get; set; }

        /// <summary>
        /// ATIACallStatusPdu
        /// ATIARepeaterStatePdu
        /// </summary>
        [JsonProperty(PropertyName = "Opcode")]
        [Name("Descriptor_Opcode")]
        public string Opcode { get; set; }

        [JsonProperty(PropertyName = "Source Unique Id")]
        [Name("Descriptor_SourceUniqueId")]
        public int SourceUniqueId { get; set; }

        [JsonProperty(PropertyName = "Destination Unique Id")]
        [Name("Descriptor_DestinationUniqueId")]
        public int DestinationUniqueId { get; set; }

        [JsonProperty(PropertyName = "Sequence Number")]
        [Name("Descriptor_SequenceNumber")]
        public int SequenceNumber { get; set; }

        [JsonProperty(PropertyName = "Timestamp")]
        [Name("Descriptor_Timestamp")]
        public long Timestamp { get; set; }

        [JsonProperty(PropertyName = "Fragment")]
        [Name("Descriptor_Fragment")]
        public int Fragment { get; set; }

        [JsonProperty(PropertyName = "Version")]
        [Name("Descriptor_Version")]
        public int Version { get; set; }

        [JsonProperty(PropertyName = "Role Info")]
        [Name("Descriptor_RoleInfo")]
        public string RoleInfo { get; set; }
    }
}
