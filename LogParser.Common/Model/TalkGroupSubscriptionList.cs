﻿using Newtonsoft.Json;

namespace LogParser.Common.Model
{
    public class TalkGroupSubscriptionList
    {
        [JsonProperty(PropertyName = "Talkgroup")]
        public int Talkgroup { get; set; }

        [JsonProperty(PropertyName = "Subscription Info")]
        public string SubscriptionInfo { get; set; }
    }
}
