﻿using CsvHelper.Configuration.Attributes;
using LogParser.Common.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogParser.Common.Model
{
    public class Payload
    {
        [JsonProperty(PropertyName = "State")]
        [Name("Payload_State")]
        public string State { get; set; }

        [JsonProperty(PropertyName = "Value")]
        [Name("Payload_Value")]
        public int Value { get; set; }

        [JsonProperty(PropertyName = "Timestamp")]
        [Name("Payload_Timestamp")]
        public long Timestamp { get; set; }

        [JsonProperty(PropertyName = "Logical Channel Number")]
        [Name("Payload_Logical_Channel_Number")]
        public int LogicalChannelNumber { get; set; }

        [JsonProperty(PropertyName = "Src")]
        [Name("Payload_Src")]
        public int Src { get; set; }

        [JsonProperty(PropertyName = "Tgt")]
        [Name("Payload_Tgt")]
        public int Target { get; set; }

        [JsonProperty(PropertyName = "Csn")]
        [Name("Payload_Csn")]
        public int Csn { get; set; }

        [JsonProperty(PropertyName = "Site all call site id")]
        [Name("Payload_SiteAllCallSiteId")]
        public int SiteAllCallSiteId { get; set; }

        /// <summary>
        /// group_voice_call
        /// individual_data_call
        /// individual_voice_call
        /// individual_voice_interrupt
        /// call alert
        /// deregistration
        /// group_voice_interrupt
        /// registration
        /// invalid
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = "CallType")]
        [Name("Payload_CallType")]
        public CallType CallType { get; set; }

        /// <summary>
        /// grant_update
        /// granted
        /// rejected
        /// success
        /// tx_start
        /// tx_end
        /// session_end
        /// single_burst_tx
        /// invalid
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = "Action")]
        [Name("Payload_Action")]
        public CallAction Action { get; set; }

        [JsonProperty(PropertyName = "RSSI")]
        [Name("Payload_Rssi")]
        public string Rssi { get; set; }

        /// <summary>
        /// not_applicable
        /// data_unconfirmed
        /// data_L2_ack
        /// data_sarq
        /// service_accepted
        /// service_not_authorized
        /// site_not_authorized
        /// target_busy
        /// target_not_present
        /// target_not_registered,
        /// target_radio_alerting,
        /// target_radio_unknown
        /// recipient_refused
        /// source_radio_unknown
        /// </summary>
        [JsonProperty(PropertyName = "Info")]
        [Name("Payload_Info")]
        public CallInfo Info { get; set; }

        [JsonProperty(PropertyName = "Originating Site Id")]
        [Name("Payload_OriginatingSiteId")]
        public int OriginatingSiteId { get; set; }

        [JsonProperty(PropertyName = "Talkgroup Subscription List")]
        [Name("Payload_TalkGroupSubscriptionList")]
        public List<TalkGroupSubscriptionList> TalkGroupSubscriptionList { get; set; }

        /// <summary>
        /// Unknown
        /// </summary>
        [JsonProperty(PropertyName = "Radio Type")]
        [Name("Payload_RadioType")]
        public string RadioType { get; set; }

        [JsonProperty(PropertyName = "Participating sites")]
        [Name("Payload_ParticipatingSites")]
        public List<ParticipatingSite> ParticipatingSites { get; set; }
    }
}
