﻿using CsvHelper.Configuration.Attributes;
using System;

namespace LogParser.Common.Model
{
    public class LogItem
    {

        [Name("DateTime")]
        public DateTime Waktu { get; set; }
        public LogData Data { get; set; }
    }

    public class LogData
    {
        public Source Source { get; set; }
        public Descriptor Descriptor { get; set; }
        public Payload Payload { get; set; }
    }
}
