﻿namespace LogParser.Common.Enum
{
    public enum CallType
    {
        group_voice_call,
        individual_voice_call,
        individual_data_call,
        individual_voice_interrupt,
        call_alert,
        deregistration,
        group_voice_interrupt,
        registration,
        invalid
    }
}
