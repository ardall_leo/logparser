﻿namespace LogParser.Common.Enum
{
    public enum CallAction
    {
        grant_update,
        granted,
        rejected,
        success,
        tx_start,
        tx_end,
        session_end,
        single_burst_tx,
        invalid
    }
}
