﻿namespace LogParser.Common.Enum
{
    public enum CallInfo
    {
        not_applicable,
        data_unconfirmed,
        data_L2_ack,
        data_sarq,
        service_accepted,
        service_not_authorized,
        site_not_authorized,
        source_radio_unknown,
        target_busy,
        target_not_present,
        target_not_registered,
        target_radio_alerting,
        target_radio_unknown,
        recipient_refused
    }
}
