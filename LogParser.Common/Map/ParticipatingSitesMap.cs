﻿using CsvHelper.Configuration;
using LogParser.Common.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace LogParser.Common.Map
{
    public class LogItemMap: ClassMap<LogItem>
    {
        public LogItemMap()
        {
            AutoMap();
            Map(m => m.Data.Payload.ParticipatingSites).Name("Payload_ParticipatingSites").ConvertUsing(o =>  JsonConvert.SerializeObject(o.Data.Payload.ParticipatingSites));
            Map(m => m.Data.Payload.TalkGroupSubscriptionList).Name("Payload_TalkGroupSubscriptionList").ConvertUsing(o => JsonConvert.SerializeObject(o.Data.Payload.TalkGroupSubscriptionList));
        }
    }



}
