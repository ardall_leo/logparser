﻿using LogParser.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LogParser.Common.Report
{
    public class Report
    {
        private List<LogItem> _logItems;
        public Report(List<LogItem> logItems)
        {
            _logItems = logItems;
        }
        
        public IEnumerable<dynamic> GroupByDay => _logItems
            .GroupBy(x => new { x.Waktu.Day, x.Waktu.Month, x.Waktu.Year});

        public IEnumerable<Call> ListOfCall => _logItems
            .Where(item => item.Data.Payload.Csn > 0)
            .Select(y => new Call
            {
                Action = y.Data.Payload.Action,
                CallType = y.Data.Payload.CallType,
                Csn = y.Data.Payload.Csn,
                DeviceSiteId = y.Data.Source.DeviceSiteId,
                RepeaterId = y.Data.Source.RepeaterId,
                Source = y.Data.Payload.Src,
                Target = y.Data.Payload.Target,
                Logical = y.Data.Payload.LogicalChannelNumber
                });

        public IEnumerable<Call> ListOfCall2 => _logItems
            .Where(item => item.Data.Payload.Csn > 0)
            .GroupBy(item => new
            {
                item.Data.Payload.Csn
            })
            .Select(y => new Call
            {
                TimeStart = y.Min(x => x.Waktu),
                TimeEnd = y.Any(x => x.Data.Payload.Action == Enum.CallAction.rejected) ? default(DateTime) : y.Max(x => x.Waktu),
                CallStatus = y.Any(x => x.Data.Payload.Action == Enum.CallAction.rejected) ? Enum.CallStatus.failed : Enum.CallStatus.success,
                CallType = y.FirstOrDefault().Data.Payload.CallType,
                Csn = y.FirstOrDefault().Data.Payload.Csn,
                DeviceSiteId = y.Where(x => x.Data.Payload.Action != Enum.CallAction.granted && x.Data.Payload.Action != Enum.CallAction.grant_update).FirstOrDefault()?.Data?.Source?.DeviceSiteId,
                RepeaterId = y.Where(x => x.Data.Payload.Action != Enum.CallAction.granted && x.Data.Payload.Action != Enum.CallAction.grant_update).FirstOrDefault()?.Data?.Source?.RepeaterId,
                Source = y.FirstOrDefault().Data.Payload.Src,
                Target = y.FirstOrDefault().Data.Payload.Target,
                RepeaterSlots = y.FirstOrDefault().Data.Source.RepeaterSlots,
                Rssi = y.FirstOrDefault().Data.Payload.Rssi
            });

        public IEnumerable<CallStats> GetStats {
            get
            {
                var numberOfCall = ListOfCall2
                .GroupBy(c => new {
                    c.TimeStart.Date,
                    c.DeviceSiteId,
                    c.CallType,
                    c.CallStatus
                })
                .Select(c => new CallStats
                {
                    Date = c.Key.Date,
                    DeviceSiteId = c.Key.DeviceSiteId,
                    CallType = c.Key.CallType,
                    CallStatus = c.Key.CallStatus,
                    Total = c.Count()
                }).ToList();

                return numberOfCall;
            }
        }
    }
}
