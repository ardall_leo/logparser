﻿
using LogParser.Common.Model;
using Microsoft.Win32;
using Raven.Client.Documents;
using Raven.Client.Documents.Queries.Facets;
using Raven.Client.Documents.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogParser.Common.Storage
{
    public class DocumentStoreHolder
    {
        // Use Lazy<IDocumentStore> to initialize the document store lazily. 
        // This ensures that it is created only once - when first accessing the public `Store` property.
        private static Lazy<IDocumentStore> store = new Lazy<IDocumentStore>(CreateStore);

        public static IDocumentStore Store => store.Value;

        public async static void Save<T>(T entity)
        {
            using (IAsyncDocumentSession asyncSession = Store.OpenAsyncSession())
            {
                // Mark the entity for storage in the Session.
                await asyncSession.StoreAsync(entity);
                // Any changes made to the entity will now be tracked by the Session.
                // However, the changes are persisted to the server only when 'SaveChanges()' is called.
                await asyncSession.SaveChangesAsync();
                // At this point the entity is persisted to the database as a new document.
                // Since no database was specified when opening the Session, the Default Database is used.
            }
        }

        public static IEnumerable<T> GetAll<T>()
        {
            using (IDocumentSession session = Store.OpenSession())
            {
                IEnumerable<T> entities = session
                .Query<T>()
                .Lazily().Value;

                return entities;
            }
        }

        public static T GetOldestEntry<T>() where T : BaseLog
        {
            using (IDocumentSession session = Store.OpenSession())
            {
                return session
                .Query<T>()
                .OrderBy(i => i.Date)
                .FirstOrDefault();
            }
        }

        public static IEnumerable<T> GetAll<T>(DateTime startTime, DateTime endTime, int? deviceSiteId = null) where T: BaseLog
        {
            /*
            if (startTime == DateTime.MinValue)
                startTime = DateTime.Now.AddDays(-7); */

            if (endTime == DateTime.MinValue)
                endTime = DateTime.Now;

            using (IDocumentSession session = Store.OpenSession())
            {
                IQueryable<T> entities = session
                .Query<T>()
                .Where(i => i.Date >= startTime && i.Date <= endTime);

                if (deviceSiteId != null)
                {
                    entities = entities.Where(i => i.DeviceSiteId == deviceSiteId);
                }

                return entities.Lazily().Value;
            }
        }

        public static IEnumerable<T> GetAllCalls<T>(DateTime startDate, DateTime endDate, int? deviceSiteId = null) where T: Call
        {
            using (IDocumentSession session = Store.OpenSession())
            {
                IQueryable<T> entities = session
                .Query<T>()
                .Where(i => i.TimeStart > startDate && i.TimeEnd < endDate);

                if (deviceSiteId != null)
                {
                    entities = entities.Where(i => i.DeviceSiteId == deviceSiteId);
                }

                var result = entities.Lazily().Value;

                return result;
            }
        }

        public static Dictionary<string, FacetResult> GetFacet<T>() where T: CallStats
        {
            using (IDocumentSession session = Store.OpenSession())
            {
                return session.Query<T>()
                    .AggregateBy(new Facet
                    {
                        FieldName = "CallStatus",
                        Options = new FacetOptions
                        {
                            TermSortMode = FacetTermSortMode.CountAsc
                        },
                        Aggregations =
                        {
                            { FacetAggregation.Sum, "Total"}
                        }
                    })
                    .Execute();
            }
        }
        private static IDocumentStore CreateStore()
        {
            RegistryKey rk = Registry.LocalMachine;
            var dbServer = (string)Registry.LocalMachine.OpenSubKey("Software\\Carano").GetValue("DatabaseServer");
            var dbName = (string)Registry.LocalMachine.OpenSubKey("Software\\Carano").GetValue("DatabaseName");

            IDocumentStore store = new DocumentStore()
            {
                // Define the cluster node URLs (required)
                Urls = new[] { dbServer, 
                           /*some additional nodes of this cluster*/ },

                // Set conventions as necessary (optional)
                Conventions =
            {
                MaxNumberOfRequestsPerSession = 10,
                UseOptimisticConcurrency = true
            },

                // Define a default database (optional)
                Database = dbName,

                // Define a client certificate (optional)
               // Certificate = new X509Certificate2("C:\\path_to_your_pfx_file\\cert.pfx"),

                // Initialize the Document Store
            }.Initialize();

            return store;
        }
    }
}
