$path = "Deploy"

If (!(test-path $path))
{
    md Deploy
}else{
	Write-Host "Removing old builds..."
	Remove-Item .\Deploy\LogParserService.zip -Force
	Remove-Item .\Deploy\LogParserWeb.zip -Force
}

Write-Host "Building LogParser Service"
&"$PSScriptroot\LogParserService\build.ps1" | Out-Null
Write-Host "DONE." -ForegroundColor Green
Write-Host "Building LogParser Web"
&"$PSScriptroot\LogParserWeb\build.ps1" | Out-Null
Write-Host "DONE." -ForegroundColor Green

Set-Location $PSScriptroot