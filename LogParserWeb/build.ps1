$RootPath = Split-Path (Split-Path $PSScriptRoot -Parent)

function Zip($source, $target){
	Add-Type -Assembly "system.io.compression.filesystem"
	[io.compression.zipfile]::CreateFromDirectory($source,$target)
}

function Clean-Up
{
	Remove-Item "$($PSScriptRoot)\bin\Release\netcoreapp3.0\win-x64\publish" -Recurse
}

Write-Host "Cleaning Up..."
Clean-Up

Set-Location "$($PSScriptRoot)\ClientApp"

Write-Host "Download npm packages"
npm install --silent

Write-Host "Building UI"
npm run build

Write-Host "Copy UI dist to wwwroot"
Copy-Item "$($PSScriptRoot)\ClientApp\dist\*" -Destination "$($PSScriptRoot)\wwwroot" -Recurse -force

Set-Location $PSScriptRoot
Write-Host "Running dotnet publish"
dotnet publish -c Release -r win-x64 --self-contained true

Set-Location "$($PSScriptRoot)\bin\Release\netcoreapp3.0\win-x64\publish"

Write-Host "Compressing..."
Zip "$($PSScriptRoot)\bin\Release\netcoreapp3.0\win-x64\publish" "$($RootPath)\logparser\Deploy\LogParserWeb.zip"
Set-Location $PSScriptRoot