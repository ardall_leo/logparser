﻿using CsvHelper;
using LogParser.Common.Model;
using LogParser.Common.Storage;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Raven.Client.Documents.Queries.Facets;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LogParserWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController
    {
        public readonly List<DeviceSite> deviceSites;
        public ReportController(List<DeviceSite> deviceSites)
        {
            this.deviceSites = deviceSites;
        }

        [HttpGet("all")]
        public ActionResult<Call> GetAll(long id)
        {
            return new JsonResult(DocumentStoreHolder.GetAll<Call>());
        }

        [HttpGet("byDate")]
        public ActionResult<Call> GetAll()
        {
            return new JsonResult(
                DocumentStoreHolder.GetAllCalls<Call>(DateTime.Parse("2019-05-25T11:49:45"), DateTime.Parse("2019-05-25T11:59:45"))
                .Where(p => p.CallStatus == LogParser.Common.Enum.CallStatus.failed)
            );
        }

        [HttpGet("getOldestData")]
        public ActionResult<Call> GetAllOldestData()
        {
            return new JsonResult(
                DocumentStoreHolder.GetOldestEntry<CallStats>()?.Date.Year
            );
        }

        [HttpGet("getCSV")]
        public FileStreamResult GetCSV(string groupBy = "day", string deviceSiteId = null, string startDate = null, string endDate = null)
        {
            var result = WriteCsvToMemory(_GetStats(groupBy, deviceSiteId, startDate, endDate));
            var memoryStream = new MemoryStream(result);
            return new FileStreamResult(memoryStream, "text/csv") { FileDownloadName = "callstats.csv" };
        }

        [HttpGet("getRawCallCSV")]
        public FileStreamResult getRawCallCSV(string groupBy = "day", string deviceSiteId = null, string startDate = null, string endDate = null)
        {
            var result = WriteCsvToMemory(_GetCallStats(groupBy, deviceSiteId, startDate, endDate));
            var memoryStream = new MemoryStream(result);
            return new FileStreamResult(memoryStream, "text/csv") { FileDownloadName = "raw-calls.csv" };
        }

        public byte[] WriteCsvToMemory<T>(IEnumerable<T> records)
        {
            using (var memoryStream = new MemoryStream())
            using (var streamWriter = new StreamWriter(memoryStream))
            using (var csvWriter = new CsvWriter(streamWriter, CultureInfo.InvariantCulture))
            {
                csvWriter.WriteRecords(records);
                streamWriter.Flush();
                return memoryStream.ToArray();
            }
        }

        [HttpGet("stats")]
        public ActionResult<CallStats> GetStats(string groupBy = "day", string deviceSiteId = null, string startDate = null, string endDate = null)
        {
            return new JsonResult(_GetStats(groupBy, deviceSiteId, startDate, endDate));
        }

        private static IEnumerable<dynamic> _GetCallStats(string groupBy, string deviceSiteId, string startDate, string endDate)
        {
            DateTime _startDate, _endDate;
            DateTime.TryParse(startDate, out _startDate);
            DateTime.TryParse(endDate, out _endDate);
            int? siteId = null;

            if (!string.IsNullOrEmpty(deviceSiteId))
                siteId = Convert.ToInt32(deviceSiteId);

            return DocumentStoreHolder.GetAllCalls<Call>(_startDate, _endDate, siteId);
        }

        private static IEnumerable<dynamic> _GetStats(string groupBy, string deviceSiteId, string startDate, string endDate)
        {
            DateTime _startDate, _endDate;
            DateTime.TryParse(startDate, out _startDate);
            DateTime.TryParse(endDate, out _endDate);
            int? siteId = null;

            if (!string.IsNullOrEmpty(deviceSiteId))
                siteId = Convert.ToInt32(deviceSiteId);

            var callStats = DocumentStoreHolder.GetAll<CallStats>(_startDate, _endDate, siteId);

            switch (groupBy)
            {
                default:
                    return callStats.OrderBy(i => i.Date)
                    .GroupBy(i => new
                    {
                        i.Date.Day,
                        i.Date.Month,
                        i.Date.Year,
                        i.Date,
                        i.DeviceSiteId,
                        i.CallType,
                        i.CallStatus
                    })
                    .Select(i => new
                    {
                        i.Key.Day,
                        i.Key.Month,
                        i.Key.Year,
                        i.Key.Date,
                        i.Key.DeviceSiteId,
                        i.Key.CallType,
                        i.Key.CallStatus,
                        Total = i.Sum(j => j.Total)
                    })
                    ;
                case "month":
                    return callStats.GroupBy(i => new
                    {
                        i.Date.Month,
                        i.Date.Year,
                        i.DeviceSiteId,
                        i.CallType,
                        i.CallStatus
                    })
                    .Select(i => new
                    {
                        i.Key.Month,
                        i.Key.Year,
                        i.Key.DeviceSiteId,
                        i.Key.CallType,
                        i.Key.CallStatus,
                        Total = i.Sum(j => j.Total)
                    })
                    ;
                case "year":
                    return callStats.GroupBy(i => new
                    {
                        i.Date.Year,
                        i.DeviceSiteId,
                        i.CallType,
                        i.CallStatus
                    })
                    .Select(i => new
                    {
                        i.Key.Year,
                        i.Key.DeviceSiteId,
                        i.Key.CallType,
                        i.Key.CallStatus,
                        Total = i.Sum(j => j.Total)
                    })
                    ;
            }
        }

        [HttpGet("facets")]
        public ActionResult<Dictionary<string, FacetResult>> GetFacet()
        {
            return new JsonResult(
                DocumentStoreHolder.GetFacet<CallStats>()
            );
        }
    }
}
