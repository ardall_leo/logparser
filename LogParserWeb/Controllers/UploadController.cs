﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogParserWeb.Controllers
{
    public class UploadController : Controller
    {
        public IActionResult Index()
        {

            return View();
        }

        [HttpPost("")]
        public IActionResult Upload()
        {
            return new JsonResult(new { });
        }
    }
}
