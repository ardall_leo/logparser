﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LogParserWeb.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace LogParserWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEnumerable<DeviceSite> deviceSitesList;

        public HomeController(IConfiguration config)
        {
            deviceSitesList = config.GetSection("DeviceSitesId")
                .GetChildren()
                .ToList()
                .Select(x => new DeviceSite
                {
                    Id = x.GetValue<int>("id"),
                    Name = x.GetValue<string>("name")
                });
        }

        public IActionResult Index()
        {
            var serializerSettings = new JsonSerializerSettings()
            {
                StringEscapeHandling = StringEscapeHandling.EscapeHtml
            };

            ViewData["deviceSites"] = JsonConvert.SerializeObject(this.deviceSitesList.ToList(), serializerSettings);

            return View();
        }
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
