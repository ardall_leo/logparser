﻿using LogParser.Common.Model;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace LogParserWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SettingsController
    {
        private const string DEVICE_MAPPING_FILE = "device-mapping.json";

        public readonly List<DeviceSite> deviceSites;
        public SettingsController(List<DeviceSite> deviceSites)
        {
            this.deviceSites = deviceSites;
        }

        [HttpGet("getDevicesMapping")]
        public ActionResult<List<DeviceSite>> GetDevicesMapping()
        {
            return new JsonResult(this.deviceSites);
        }

        [HttpPost("saveDevicesMapping")]
        public ActionResult SaveDevicesMapping([FromBody] JObject data)
        {
            try
            {
                var input = data.ToObject<DeviceSite>();

                this.deviceSites.Find(i => i.Id == input.Id).Name = input.Name;

                File.WriteAllText(DEVICE_MAPPING_FILE, JsonConvert.SerializeObject(this.deviceSites, Formatting.Indented));

                return new JsonResult("Success")
                {
                    StatusCode = (int)HttpStatusCode.OK
                };
            }
            catch(Exception ex)
            {
                return new JsonResult("Failed")
                {
                    StatusCode = (int)HttpStatusCode.BadRequest
                };
            }
        }
    }
}
