using System;

namespace LogParserWeb.Models
{
    public class DeviceSite
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}