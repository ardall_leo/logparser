﻿export class HttpError extends Error {
  msg;
  constructor(msg) {
    super();
    this.msg = msg;
  }
}