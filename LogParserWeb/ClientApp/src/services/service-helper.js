﻿export function parseError(error) {
  if (!(error instanceof Error)) {
    return new Promise((resolve, reject) => reject(error.json()));
  }
  return undefined;
}