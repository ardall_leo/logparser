// regenerator-runtime is to support async/await syntax in ESNext.
// If you don't use async/await, you can remove regenerator-runtime.
import 'regenerator-runtime/runtime';
import * as environment from '../config/environment.json';
import {PLATFORM} from 'aurelia-pal';
import * as globalComponents from './components/registry';
import 'chart.js';
import 'bootstrap';

export function configure(aurelia) {
  aurelia.use
	  .eventAggregator()
    //.standardConfiguration()
    .defaultBindingLanguage()
    .defaultResources()
    //.plugin(PLATFORM.moduleName('jquery'))
    //.plugin(PLATFORM.moduleName('bootstrap'))
    .router()
    .history()
    .feature(PLATFORM.moduleName('components/index'));

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

  if (environment.testing) {
    aurelia.use.plugin(PLATFORM.moduleName('aurelia-testing'));
  }

  aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('my-app')));
}
