import { EventAggregator } from 'aurelia-event-aggregator';
import { inject } from 'aurelia-framework';
import { SharedState } from '../state';

@inject(SharedState, EventAggregator)
export class SideBar {
    router;
    message = 'Hello World!';

    constructor(state, ea){
        console.log('wew');
        this.state = state;
        this.ea = ea;

        this.router = this.state.router;

        this.ea.subscribe('updateRouter', () =>{
            this.router = this.state.router;
        });
    }
}