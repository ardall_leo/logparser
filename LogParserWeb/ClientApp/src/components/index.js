import { PLATFORM } from 'aurelia-pal';

export function configure(config) {
    config.globalResources([
        PLATFORM.moduleName('./nav-bar'),
        PLATFORM.moduleName('./side-bar'),
        PLATFORM.moduleName('./total-count-widget'),
        PLATFORM.moduleName('./call-group'),
        PLATFORM.moduleName('./call-distribution'),
        PLATFORM.moduleName('../dashboard'),
        PLATFORM.moduleName('../settings'),
	]);
}
