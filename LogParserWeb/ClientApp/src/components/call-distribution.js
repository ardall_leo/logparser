import { EventAggregator } from 'aurelia-event-aggregator';
import { inject } from 'aurelia-framework';
import { SharedState } from '../state';
import axios from 'axios';

@inject(SharedState, EventAggregator)
export class CallDistribution{
    constructor(state,ea){
        this.ea = ea;
        this.state = state;
        
        this.http = axios.create({
            baseURL: 'http://localhost:3000/api/'
        });
    }

    attached(){
        this.buildPieChart();
    }

    buildPieChart(){
        this.ea.subscribe('updatePieChart', (data) =>{
            console.log(data);

            let getCallDistribution = (callType, callStatus) =>
            {
                return data.filter(d => d.callType == callType && d.callStatus == callStatus)
                .map(d => d.total)
                .reduce(function (x, y) { return x + y; }, 0);
            }

            let newDataSets = [{
                data: [
                    getCallDistribution(1,1),
                    getCallDistribution(0,1),
                    getCallDistribution(1,0),
                    getCallDistribution(0,0)
                ],
                backgroundColor: [
                    window.chartColors.green,
                    window.chartColors.blue,
                    window.chartColors.red,
                    window.chartColors.orange,
                ],
                label: 'Dataset 1'
            }];

            window.myDoughnut.data.datasets = newDataSets;
            window.myDoughnut.update();
        });

        var config = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [0,0,0,0],
                    backgroundColor: [
                        window.chartColors.green,
                        window.chartColors.blue,
                        window.chartColors.red,
                        window.chartColors.orange
                    ],
                    label: 'Dataset 1'
                }],
                labels: [
                    'Individual Call (Success)',
                    'Group Call (Success)',
                    'Individual Call (Failed)',
                    'Group Call (Failed)'
                ]
            },
            options: {
                responsive: true,
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
                legend: {
                    position:'top',
                    labels: {
                        usePointStyle: true,
                        fontSize: 12,
                        fontFamily: "'Poppins', sans-serif",
                        fontColor: '#000',
                        fontStyle: '500',
    
                    }
                },
            }
        };

        var ctx = document.getElementById('doughnut-chart').getContext('2d');
        window.myDoughnut = new Chart(ctx, config);
    }
}