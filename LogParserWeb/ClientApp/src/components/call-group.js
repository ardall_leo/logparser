import { EventAggregator } from 'aurelia-event-aggregator';
import { inject } from 'aurelia-framework';
import { SharedState } from '../state';
import { CallType } from '../constants/CallType';
import {bindable} from 'aurelia-framework';
import * as moment from 'moment';
import axios from 'axios';
import 'bootstrap-daterangepicker';
import 'bootstrap-select';
import 'bootstrap-notify';

@inject(SharedState, EventAggregator, Element)
export class CallGroup {
    @bindable deviceSiteId;
    siteIds = [];

    dailyChartData;
    monthlyChartData;
    yearlyChartData;
    componentCount = 0;

    dailyChart;
    yearlyChart;
    monthlyChart;

    fetching = false;

    startDate = moment().startOf('year');;
    endDate = moment().endOf('year');

    oldestYear = 2019;
    latestYear = moment().year();
    _years = [];

    selectedYears = [];

    months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    years = [];

    get allYears(){
        if (this._years.length == 0){
            for(let i=this.oldestYear; i <= this.latestYear; i++){
                this._years.push(i);
            }
        }

        return this._years;
    }

    getActiveViewBy(){
        var selectedViewBy = this.el.querySelector('#nav-time-tab a.active').text;
        switch(selectedViewBy.toLowerCase()){
            case 'daily':
                return 'day';
            case 'yearly':
                return 'year';
            default:
                return 'month'
        }
    }

    getOldestData(){
        let _this = this;

        if (localStorage.getItem('oldestData')){
            this.oldestYear = localStorage.getItem('oldestData');
        } else {
            this.http.get(`report/getOldestData`)
            .then((res) => {
                if (res.status == 200) {
                    _this.oldestYear = res.data;
                }
            })
            .catch((err) => console.log(err));
        }
    }

    getSelectedYear(){
        return moment.year();
    }

    constructor(state, ea, el) {
        this.state = state;
        this.ea = ea;
        this.el = el;
        
        this.http = axios.create({
            baseURL: '/api/'
        });
        
        this.getOldestData();
    }

    attached() {
        let _this = this;

        this.getDeviceMapping();
        
        this.buildMonthlyChart();
        this.buildYearlyChart();
        this.buildDailyChart();

        this.beautify();

        // selectpicker
        $('.selectpicker').selectpicker();

        // selecting years, should reset the start date and end date
        // and set the start date to be early of the year and end date to be end of the year
        $('.selectpicker').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            let selectedItems = Array.from(document.querySelectorAll('#viewByYear .dropdown-menu li.selected')).map(x => x.innerText);

            let loadBySelectedYears = () => {
                let selectedItems = Array.from(document.querySelectorAll('#viewByYear .dropdown-menu li.selected')).map(x => x.innerText);

                _this.selectedYears = selectedItems.filter(y => y != 'All');
                _this.startDate = moment(_this.selectedYears[0], 'YYYY').startOf('year');
                _this.endDate = moment(_this.selectedYears[_this.selectedYears.length -1], 'YYYY').endOf('year');
                
                _this.fetching = true;
                _this.loadData(_this.deviceSiteId, _this.startDate.format('YYYY-MM-DD'), _this.endDate.format('YYYY-MM-DD'));
            }

            $('.card-date-filter .daterange-picker span').html("");
            if (clickedIndex == 0 && isSelected) {
                $('.selectpicker').selectpicker('selectAll');
            } else if ((clickedIndex == 0 && !isSelected) || (selectedItems.length == 1 && selectedItems[0] == "All")) {
                $('.selectpicker').selectpicker('deselectAll');
            } else if (selectedItems.length == 0) {
                _this.monthlyChart.data.datasets = [];
                _this.monthlyChart.update();

                _this.yearlyChart.data.datasets = [];
                _this.yearlyChart.update();

                _this.selectedYears = [];
                _this.startDate = null;
                _this.endDate = null;
            } else {
                loadBySelectedYears();
            }
        });
    }

    getDeviceMapping(){
        let _this = this;
        let endpoint = 'settings/getDevicesMapping';

        this.http.get(`${endpoint}`)
            .then((res) => {
                if (res.status == 200) {
                    this.siteIds = this.state.siteIds = res.data;
                    
                    $('.selectpicker-site').selectpicker();
                    $('.selectpicker-site').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
                        _this.deviceSiteId =  $('.selectpicker-site').selectpicker('val');

                        if (_this.startDate == null || _this.endDate == null) {
                            this.fetching = false;

                            $.notify({
                                // options
                                message: 'Please pick date interval first.'
                            }, {
                                // settings
                                type: 'danger',
                                placement: {
                                    from: "bottom",
                                    align: "right"
                                },
                                animate: {
                                    enter: 'animated fadeOutUp',
                                    exit: 'animated fadeInDown'
                                },
                            });
                        } else {
                            _this.loadData(_this.deviceSiteId, _this.startDate.format('YYYY-MM-DD'), _this.endDate.format('YYYY-MM-DD'));
                        }
                    });
                }
            })
            .catch((err) => this.handleError(err));
    }

    handleError(err) {
        console.log(err);

        $.notify({
            // options
            message: 'There was a problem occurs during the request, please check the logs'
        }, {
            // settings
            type: 'danger',
            placement: {
                from: "bottom",
                align: "right"
            },
        });
    }

    getStats(siteId, groupBy, startDate, endDate) {
        let endpoint = `report/stats?groupBy=${groupBy}&deviceSiteId=${siteId}&startDate=${startDate}&endDate=${endDate}`;
        let _this = this;

        return new Promise((resolve, reject) => {
            this.http.get(`${endpoint}`)
                .then((res) => {
                    if (res.status == 200) {
                        resolve(res.data);
                    }
                })
                .catch((err) => reject(err));
        });
    }

    getCSV(siteId, groupBy, startDate, endDate){
        let endpoint = `report/getCSV?groupBy=${groupBy}&deviceSiteId=${siteId}&startDate=${startDate}&endDate=${endDate}`;
        let _this = this;

        return new Promise((resolve, reject) => {
            this.http.get(`${endpoint}`)
                .then((res) => {
                    if (res.status == 200) {
                        var blob=new Blob([res.data]);
                        var link=document.createElement('a');
                        link.href=window.URL.createObjectURL(blob);
                        link.download="data-log.csv";
                        link.click();

                        _this.fetching = false;

                        resolve(res.data);
                    }
                })
                .catch((err) => reject(err));
        });
    }

    getRawCallCSV(siteId, groupBy, startDate, endDate){
        let endpoint = `report/getRawCallCSV?groupBy=${groupBy}&deviceSiteId=${siteId}&startDate=${startDate}&endDate=${endDate}`;
        let _this = this;

        
        return new Promise((resolve, reject) => {
            this.http.get(`${endpoint}`)
                .then((res) => {
                    if (res.status == 200) {
                        var blob=new Blob([res.data]);
                        var link=document.createElement('a');
                        link.href=window.URL.createObjectURL(blob);
                        link.download="raw-calls.csv";
                        link.click();

                        _this.fetching = false;

                        resolve(res.data);
                    }
                })
                .catch((err) => reject(err));
        });
    }

    loadData(siteId, startDate, endDate){
        let viewBy = this.getActiveViewBy();
        let _this = this;

        this.deviceSiteId = siteId;

        if (isNaN(siteId)) {
            this.fetching = false;

            $.notify({
                // options
                message: 'Please pick device site id first.'
            }, {
                // settings
                type: 'danger',
                placement: {
                    from: "bottom",
                    align: "right"
                },
                animate: {
                    enter: 'animated fadeOutUp',
                    exit: 'animated fadeInDown'
                },
            });
        } else {
            this.getStats(this.deviceSiteId, viewBy, startDate, endDate)
                .then(data => {
                    switch (viewBy) {
                        case 'day':
                            this.dailyChartData = _this.constructdailyChart(siteId, data);
                            break;
                    }

                    this.monthlyChartData = _this.constructMonthlyChart(siteId, data);
                    this.yearlyChartData = _this.constructYearlyChart(siteId, data);

                    this.ea.publish('updatePieChart', data);
                })
                .then(() => {
                    switch (viewBy) {
                        case 'day':
                            this.dailyChart.data.labels = this.dailyChartData.labels;
                            this.dailyChart.data.datasets = this.dailyChartData.datasets;

                            this.dailyChart.update();
                            break;
                        case 'year':
                            this.yearlyChart.data.labels = this.yearlyChartData.labels;
                            this.yearlyChart.data.datasets = this.yearlyChartData.datasets;

                            this.yearlyChart.update();
                            break;
                        default:
                            this.monthlyChart.data.labels = this.monthlyChartData.labels;
                            this.monthlyChart.data.datasets = this.monthlyChartData.datasets;

                            this.monthlyChart.update();
                            break;
                    }

                    this.fetching = false;
                });
        }
    }

    beautify() {
        let _this = this;
        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb1(startDate, endDate) {
            $('.selectpicker').selectpicker('deselectAll');

            _this.startDate = startDate;
            _this.endDate = endDate;

            let dateRangeText = startDate.format('MMMM D, YYYY') + ' - ' + endDate.format('MMMM D, YYYY');
            _this.el.querySelector('[data-device-id] .daterange-picker span').innerHTML = dateRangeText;
            
            _this.loadData(_this.deviceSiteId, startDate.format('YYYY-MM-DD'), endDate.format('YYYY-MM-DD'));
        }

        $(`call-group .daterange-picker`).daterangepicker({
            startDate: start,
            endDate: end,
            locale: { cancelLabel: 'Clear' },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Last Year': [moment().subtract(1, 'year').startOf('month'), moment().subtract(1, 'year').endOf('year')]
            }
        }, cb1);
    }

    buildDailyChart() {
        var ctx = this.el.querySelector('#portal-daily-highlights');
        this.dailyChart = new Chart(ctx, {
            type: 'bar',
            data: this.dailyChartData,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    align: 'center',
                    labels: {
                        usePointStyle: true,
                        fontSize: 12,
                        fontFamily: "'Poppins', sans-serif",
                        fontColor: '#585E7F',
                        fontStyle: '400',

                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            drawBorder: false,
                            display: false,
                            color: '#ced1e7',
                            borderDash: [2, 5],
                            zeroLineWidth: 1,
                            zeroLineColor: '#ced1e7',
                            zeroLineBorderDash: [2, 5],
                        },
                        stacked: true,
                        categoryPercentage: 0.5,
                        ticks: {
                            fontSize: 13,
                            fontFamily: "'Poppins', sans-serif",
                            fontColor: '#C3C5D4',
                            fontStyle: '400',
                            stepSize: 20,
                            padding: 10,
                            display:false
                        }

                    }],
                    yAxes: [{
                        ticks: {
                            suggestedMax: 100,
                            fontSize: 12,
                            fontFamily: "'Poppins', sans-serif",
                            fontColor: '#C3C5D4',
                            fontStyle: '400'
                        },
                        stacked: true,
                        gridLines: {
                            drawBorder: false,
                            display: true,
                            color: '#ced1e7',
                            borderDash: [2, 5],
                            zeroLineWidth: 1,
                            zeroLineColor: '#ced1e7',
                            zeroLineBorderDash: [2, 5]
                        },
                    }]
                },
                tooltips: {
                    backgroundColor: '#4765FF',
                    bodyFontFamily: 'Poppins',
                    bodyFontColor: '#FFF',
                    bodyFontSize: 12,
                    displayColors: false,
                    intersect: false,
                },
            }
        });
    }

    buildMonthlyChart() {
        let ctx = this.el.querySelector('#portal-monthly-highlights').getContext("2d");
        this.monthlyChart = new Chart(ctx, {
            type: 'bar',
            data: this.monthlyChartData,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    align: 'center',
                    labels: {
                        usePointStyle: true,
                        fontSize: 12,
                        fontFamily: "'Poppins', sans-serif",
                        fontColor: '#585E7F',
                        fontStyle: '400',

                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            drawBorder: false,
                            display: false,
                            color: '#ced1e7',
                            borderDash: [2, 5],
                            zeroLineWidth: 1,
                            zeroLineColor: '#ced1e7',
                            zeroLineBorderDash: [2, 5],
                        },
                        stacked: true,
                        categoryPercentage: 0.5,
                        ticks: {
                            fontSize: 13,
                            fontFamily: "'Poppins', sans-serif",
                            fontColor: '#C3C5D4',
                            fontStyle: '400',
                            stepSize: 20,
                            padding: 10
                        }

                    }],
                    yAxes: [{
                        ticks: {
                            suggestedMax: 100,
                            fontSize: 12,
                            fontFamily: "'Poppins', sans-serif",
                            fontColor: '#C3C5D4',
                            fontStyle: '400'
                        },
                        stacked: true,
                        gridLines: {
                            drawBorder: false,
                            display: true,
                            color: '#ced1e7',
                            borderDash: [2, 5],
                            zeroLineWidth: 1,
                            zeroLineColor: '#ced1e7',
                            zeroLineBorderDash: [2, 5]
                        },
                    }]
                },
                tooltips: {
                    backgroundColor: '#4765FF',
                    bodyFontFamily: 'Poppins',
                    bodyFontColor: '#FFF',
                    bodyFontSize: 12,
                    displayColors: false,
                    intersect: false,
                },
            }
        });
    }

    buildYearlyChart() {
        let ctx = this.el.querySelector('#portal-yearly-highlights').getContext('2d');
        this.yearlyChart = new Chart(ctx, {
            type: 'bar',
            data: this.yearlyChartData,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    align: 'center',
                    labels: {
                        usePointStyle: true,
                        fontSize: 12,
                        fontFamily: "'Poppins', sans-serif",
                        fontColor: '#585E7F',
                        fontStyle: '400',

                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            drawBorder: false,
                            display: false,
                            color: '#ced1e7',
                            borderDash: [2, 5],
                            zeroLineWidth: 1,
                            zeroLineColor: '#ced1e7',
                            zeroLineBorderDash: [2, 5],
                        },
                        stacked: true,
                        categoryPercentage: 0.5,
                        ticks: {
                            fontSize: 13,
                            fontFamily: "'Poppins', sans-serif",
                            fontColor: '#C3C5D4',
                            fontStyle: '400',
                            stepSize: 20,
                            padding: 10
                        }

                    }],
                    yAxes: [{
                        ticks: {
                            suggestedMax: 100,
                            fontSize: 12,
                            fontFamily: "'Poppins', sans-serif",
                            fontColor: '#C3C5D4',
                            fontStyle: '400'
                        },
                        stacked: true,
                        gridLines: {
                            drawBorder: false,
                            display: true,
                            color: '#ced1e7',
                            borderDash: [2, 5],
                            zeroLineWidth: 1,
                            zeroLineColor: '#ced1e7',
                            zeroLineBorderDash: [2, 5]
                        },
                    }]
                },
                tooltips: {
                    backgroundColor: '#4765FF',
                    bodyFontFamily: 'Poppins',
                    bodyFontColor: '#FFF',
                    bodyFontSize: 12,
                    displayColors: false,
                    intersect: false,
                },
            }
        });
    }

    constructYearlyChart(deviceSiteId, data) {
        let _this = this;
        var siteId0 = data.filter(i => i.deviceSiteId == deviceSiteId && i.year >= _this.startDate.year() && i.year <= _this.endDate.year());
        
        let years = Array.from(new Set(data.map(x => x.year)));

        let getData = (type) => {
            return years.map(m => {
                let found;
                switch (type) {
                    case CallType.IndividualCallSuccess:
                        found = siteId0.find(d => d.callType == 1 && d.callStatus == 1 && d.year == m);

                        return !!found ? found.total : 0;
                    case CallType.IndividualCallFailed:
                        found = siteId0.find(d => d.callType == 1 && d.callStatus == 0 && d.year == m)

                        return !!found ? found.total : 0;
                    case CallType.GroupCallSuccess:
                        found = siteId0.find(d => d.callType == 0 && d.callStatus == 1 && d.year == m)

                        return !!found ? found.total : 0;
                    case CallType.GroupCallFailed:
                        found = siteId0.find(d => d.callType == 0 && d.callStatus == 0 && d.year == m)

                        return !!found ? found.total : 0;
                }
            });
        };

        let chartData = {
            deviceSiteId: deviceSiteId,
            labels: years,
            datasets: [{
                label: 'Individual Call (Success)',
                data: getData(CallType.IndividualCallSuccess),
                backgroundColor: window.chartColors.green,
                borderColor: window.chartColors.green,
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Individual Call (Failed)',
                data: getData(CallType.IndividualCallFailed),
                backgroundColor: window.chartColors.red,
                borderColor:  window.chartColors.red,
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Group Call (Success)',
                data: getData(CallType.GroupCallSuccess),
                backgroundColor: window.chartColors.blue,
                borderColor: window.chartColors.blue,
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Group Call (Failed)',
                data: getData(CallType.GroupCallFailed),
                backgroundColor:  window.chartColors.orange,
                borderColor:  window.chartColors.orange,
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            }]
        };

        return chartData;
    }
    
    constructdailyChart(deviceSiteId, data){
        let _this = this;

        var siteId0 = data;

        let _startDay = this.startDate.clone();
        let _endDay = this.endDate.clone();
        let _dayDiff = _endDay.diff(_startDay, 'days');
        
        let _days = [];
        for(let i=0;i <= _dayDiff;i++){
            _days.push(_startDay.format("D-M-YYYY"));
            
            _startDay.add(1, 'days');
        }
        
        let getData = (type) => {
            return _days.map(i =>{
                if (siteId0.some(d => i == `${d.day}-${d.month}-${d.year}`)) {
                    let found;
                    switch (type) {
                        case CallType.IndividualCallSuccess:
                            found = siteId0.find(d => d.callType == 1 && d.callStatus == 1 && i == `${d.day}-${d.month}-${d.year}`)

                            return !!found ? found.total : 0;
                        case CallType.IndividualCallFailed:
                            found = siteId0.find(d => d.callType == 1 && d.callStatus == 0 && i == `${d.day}-${d.month}-${d.year}`)

                            return !!found ? found.total : 0;
                        case CallType.GroupCallSuccess:
                            found = siteId0.find(d => d.callType == 0 && d.callStatus == 1 && i == `${d.day}-${d.month}-${d.year}`)

                            return !!found ? found.total : 0;
                        case CallType.GroupCallFailed:
                            found = siteId0.find(d => d.callType == 0 && d.callStatus == 0 && i == `${d.day}-${d.month}-${d.year}`)

                            return !!found ? found.total : 0;
                    }
                } else {
                    return 0;
                }
            });
        };

        console.log(_days);
        console.log(getData(CallType.IndividualCallSuccess));
        console.log(getData(CallType.IndividualCallFailed));

        let chartData = {
            deviceSiteId: deviceSiteId,
            labels: _days,
            datasets: [{
                label: 'Individual Call (Success)',
                data: getData(CallType.IndividualCallSuccess),
                backgroundColor: window.chartColors.green,
                borderColor: window.chartColors.green,
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Individual Call (Failed)',
                data: getData(CallType.IndividualCallFailed),
                backgroundColor:  window.chartColors.red,
                borderColor:  window.chartColors.red,
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Group Call (Success)',
                data: getData(CallType.GroupCallSuccess),
                backgroundColor: window.chartColors.blue,
                borderColor: window.chartColors.blue,
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Group Call (Failed)',
                data: getData(CallType.GroupCallFailed),
                backgroundColor:  window.chartColors.orange,
                borderColor:  window.chartColors.orange,
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            }]
        };

        return chartData;
    }

    constructMonthlyChart(deviceSiteId, data) {
        let _this = this;

        var siteId0 = data.filter(i => i.deviceSiteId == deviceSiteId && i.year >= _this.startDate.year() && i.year <= _this.endDate.year());

        let getData = (type) => {
            return this.months.map(m => {
                if (siteId0.some(d => this.getMonth(d.month) == m)) {
                    let found;
                    switch (type) {
                        case CallType.IndividualCallSuccess:
                            found = siteId0.filter(d => d.callType == 1 && d.callStatus == 1)
                                .find(d => this.getMonth(d.month) == m);

                            return !!found ? found.total : 0;
                        case CallType.IndividualCallFailed:
                            found = siteId0.filter(d => d.callType == 1 && d.callStatus == 0)
                                .find(d => this.getMonth(d.month) == m)

                            return !!found ? found.total : 0;
                        case CallType.GroupCallSuccess:
                            found = siteId0.filter(d => d.callType == 0 && d.callStatus == 1)
                                .find(d => this.getMonth(d.month) == m)

                            return !!found ? found.total : 0;
                        case CallType.GroupCallFailed:
                            found = siteId0.filter(d => d.callType == 0 && d.callStatus == 0)
                                .find(d => this.getMonth(d.month) == m)

                            return !!found ? found.total : 0;
                    }
                } else {
                    return 0
                }
            })
        };

        let chartData = {
            deviceSiteId: deviceSiteId,
            labels: this.months, //["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets: [{
                label: 'Individual Call (Success)',
                data: getData(CallType.IndividualCallSuccess),
                backgroundColor: window.chartColors.green,
                borderColor: window.chartColors.green,
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Individual Call (Failed)',
                data: getData(CallType.IndividualCallFailed),
                backgroundColor:  window.chartColors.red,
                borderColor:  window.chartColors.red,
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Group Call (Success)',
                data: getData(CallType.GroupCallSuccess),
                backgroundColor: window.chartColors.blue,
                borderColor: window.chartColors.blue,
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Group Call (Failed)',
                data: getData(CallType.GroupCallFailed),
                backgroundColor:  window.chartColors.orange,
                borderColor:  window.chartColors.orange,
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            }]
        };

        return chartData;
    }
    
    redrawYearly() {
        this.fetching = true;
        this.loadData(this.deviceSiteId, this.startDate.format('YYYY-MM-DD'), this.endDate.format('YYYY-MM-DD'));
    }

    redrawMonthly() {
        this.fetching = true;
        this.loadData(this.deviceSiteId, this.startDate.format('YYYY-MM-DD'), this.endDate.format('YYYY-MM-DD'));
    }
    
    redrawDaily(){
        this.fetching = true;
        this.loadData(this.deviceSiteId, this.startDate.format('YYYY-MM-DD'), this.endDate.format('YYYY-MM-DD'));
    }

    exportToCSV(){
        this.fetching = true;
        let viewBy = this.getActiveViewBy();

        return this.getCSV(this.deviceSiteId, viewBy, this.startDate, this.endDate);
    }

    exportRawCallToCSV(){
        this.fetching = true;
        let viewBy = this.getActiveViewBy();

        return this.getRawCallCSV(this.deviceSiteId, viewBy, this.startDate.format('YYYY-MM-DD'), this.endDate.format('YYYY-MM-DD'));
    }

    getMonth(id) {
        return this.months[id - 1];
    }
}
