import { EventAggregator } from 'aurelia-event-aggregator';
import { inject } from 'aurelia-framework';
import { SharedState } from '../state';
//import { bindable } from 'aurelia-templating';
import { bindable } from 'aurelia-framework';

@inject(SharedState, EventAggregator, Element)
export class TotalCountWidget {
    @bindable chartdata = {};
    @bindable compdata = {};

    totalCall = 0;

    constructor(state, ea, el) {
        this.state = state;
        this.ea = ea;
        this.el = el;
    }

    attached() {
        this.subscribe();
    }

    subscribe() {
        let _this = this;

        this.ea.subscribe('dailyStats:loaded', (data) => {
            this.initChart();
        });
    }

    initChart() {
        var ctx = this.el.querySelector('canvas').getContext("2d");

        var gradientFill = ctx.createLinearGradient(500, 0, 100, 0);
        gradientFill.addColorStop(1, "#CFCFFF");
        gradientFill.addColorStop(0, "#FFF");

        this.chartdata.datasets.forEach((datum) => {
            datum.backgroundColor = gradientFill
        });

        var myChart = new Chart(ctx, {
            type: 'line',
            data: this.chartdata,
            options: {
                plugins: {
                    filler: {
                        propagate: false
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        display: false,
                        gridLines: {
                            display: false,
                        },
                    }],
                    xAxes: [{
                        gridLines: {
                            display: false
                        }, ticks: {
                            display: false
                        },
                    }]
                },
                layout: {
                    padding: {
                        left: -20,
                        right: 0,
                        top: 0,
                        bottom: -20
                    }
                },
                tooltips: {
                    backgroundColor: '#4765FF',
                    bodyFontFamily: 'Poppins',
                    bodyFontColor: '#FFF',
                    bodyFontSize: 12,
                    displayColors: false,
                    intersect: false,
                },
            }
        });
    }
}
