import { EventAggregator } from 'aurelia-event-aggregator';
import { inject } from 'aurelia-framework';
import { SharedState } from './state';
import axios from 'axios';
import 'bootstrap-notify';

@inject(SharedState, EventAggregator)
export class Settings {
    siteIds = [];
    deviceSiteId;

    constructor(state, ea){
        this.state = state;
        this.ea = ea;

        this.http = axios.create({
            baseURL: '/api/'
        });
    }
    
    activate(){
        
    }

    attached(){
        this.getDeviceMapping();
    }

    getDeviceMapping(){
        let _this = this;
        let endpoint = 'settings/getDevicesMapping';

        this.http.get(`${endpoint}`)
            .then((res) => {
                if (res.status == 200) {
                    this.siteIds = this.state.siteIds = res.data;
                    
                    $('.selectpicker-site').selectpicker();
                    $('.selectpicker-site').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
                        _this.deviceSiteId =  e.currentTarget.selectedOptions[0].text;
                    });
                }
            })
            .catch((err) => this.handleError(err));
    }

    handleError(err){
        console.log(err);

        $.notify({
            // options
            message: 'There was a problem occurs during the request, please check the logs' 
        },{
            // settings
            type: 'danger',
            placement: {
                from: "bottom",
                align: "right"
            },
        });
    }

    updateDeviceMapping(name){
        let endpoint = 'settings/saveDevicesMapping';

        this.http.post(`${endpoint}`,{
                id: this.deviceSiteId,
                name: name
            })
            .then((res) => {
                if (res.status == 200) {
                    $.notify({
                        // options
                        message: 'The changes has been saved.' 
                    },{
                        // settings
                        type: 'success',
                        placement: {
                            from: "bottom",
                            align: "right"
                        },
                    });
                }
            })
            .catch((err) => {
                this.handleError(err);
            });
    }
}
