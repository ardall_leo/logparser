import { EventAggregator } from 'aurelia-event-aggregator';
import { inject } from 'aurelia-framework';
import { SharedState } from './state';
import { CallType } from './constants/CallType';
import axios from 'axios';
import * as moment from 'moment';

window.chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
};

@inject(SharedState, EventAggregator)
export class Dashboard {
    message = 'Hello World!';
    totalCallChartData;
    totalCallCompData = {
        title: "Total Call today",
        totalCall: 0,
        diff: 0
    };
    groupCallData;
    totalGroupCallCompData = {};
    individualCallData;
    totalIndividualCallCompData = {};

    months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    siteIds = [];

    constructor(state,ea) {
        this.state = state;
        this.ea = ea;

        this.http = axios.create({
            baseURL: 'http://localhost:3000/api/'
        });
    }

    activate(){
    }

    attached() {
        this.getDailyStats();
    }

    getDailyStats() {
        let endpoint = 'report/stats?groupBy=day';
        let _this = this;

        this.http.get(`${endpoint}`)
            .then((res) => {
                if (res.status == 200) {
                    let allCalls = res.data.sort((a, b) => moment(a.date).format('YYYYMMDD') - moment(b.date).format('YYYYMMDD'));
                    let groupCalls = allCalls.filter(c => c.callType == 0);
                    let individualCalls = allCalls.filter(c => c.callType == 1);

                    var { totalCall, diff, weekData, chartData } = this.getTotalGroupCallData(allCalls);
                    _this.totalCallChartData = chartData;
                    _this.totalCallCompData = {
                        title: "Total Call today",
                        totalCall: totalCall,
                        diff: diff
                    };

                    var { totalCall, diff, weekData, chartData } = this.getTotalGroupCallData(groupCalls);
                    _this.groupCallData = chartData;
                    _this.totalGroupCallCompData = {
                        title: "Total Group Call today",
                        totalCall: totalCall,
                        diff: diff
                    };

                    var { totalCall, diff, weekData, chartData } = this.getTotalGroupCallData(individualCalls);
                    _this.individualCallData = chartData;
                    _this.totalIndividualCallCompData = {
                        title: "Total Individual Call today",
                        totalCall: totalCall,
                        diff: diff
                    };

                    this.state.totalCountData = {
                        totalCallCompData : {
                            title: "Total Call today",
                            totalCall: totalCall,
                            diff: diff
                        }
                    }

                    setTimeout(function(){
                        _this.ea.publish('dailyStats:loaded');
                    }, 200);
                }
            })
            .catch((err) => console.log(err));
    }

    getTotalGroupCallData(data) {
        let todayFilter = (date, isYesterday) => {
            let refDate = !isYesterday ? moment() : moment().subtract(1, "days");

            return (moment(date).format('L') == refDate.format('L'));
        };

        let totalCallToday = data
            .filter(i => todayFilter(i.date, false))
            .map(i => i.total)
            .reduce(function (x, y) { return x + y; }, 0);

        let totalCallYesterday = data
            .filter(i => todayFilter(i.date, true))
            .map(i => i.total)
            .reduce(function (x, y) { return x + y; }, 0);

        let diff = (totalCallToday - totalCallYesterday) * 100 / totalCallYesterday;

        let lastWeek = moment().subtract(1, "weeks");
        let lastWeekData = data.filter(i => moment(i.date) > lastWeek);

        let dailyData = [];
        let dateLabels = [];
        let _startDay = moment(lastWeek.format("YYYY-MM-DD"));
        for (let i = lastWeek.date(); i <= (lastWeek.date() + 7); i++) {
            dateLabels.push(_startDay.calendar(null, {
                sameDay: '[Today]',
                nextDay: '[Tomorrow]',
                nextWeek: 'dddd',
                lastDay: '[Yesterday]',
                lastWeek: '[Last] dddd',
                sameElse: 'YYYY-MM-DD h:mm:ss a'
            }));

            let _totalCallInADay = lastWeekData
                .filter(j => moment(j.date).date() == i)
                .map(i => i.total)
                .reduce(function (x, y) { return x + y; }, 0);

            _startDay.add(1, "day");
            dailyData.push(_totalCallInADay);
        }

        let totalCallDaily = data.filter(i => moment(i.date) > lastWeek).map(i => i.total);

        let chartData = {
            labels: dateLabels,
            datasets: [{
                label: "Data",
                borderColor: '#4765FF',
                pointBorderWidth: 0,
                pointHoverRadius: 0,
                pointHoverBorderWidth: 0,
                pointRadius: 0,
                fill: true,
                borderWidth: 3,
                data: dailyData
            }]
        };

        return {
            totalCall: totalCallToday,
            diff: diff,
            weekData: totalCallDaily,
            chartData: chartData
        }
    }
}
