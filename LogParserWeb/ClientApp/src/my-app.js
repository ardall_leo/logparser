import { EventAggregator } from 'aurelia-event-aggregator';
import { inject } from 'aurelia-framework';
import {PLATFORM} from 'aurelia-pal';
import { SharedState } from './state';
import { CallType } from './constants/CallType';
import axios from 'axios';
import * as moment from 'moment';

@inject(SharedState, EventAggregator)
export class MyApp {
    message = 'Hello World!';
    totalCallChartData;
    months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    siteIds = [];

    configureRouter(config, router) {
        this.state.router = this.router = router;
        config.title = 'Aurelia';
        config.map([
            { route: '', redirect: 'dashboard' },
            { route: 'dashboard', nav: true, name: 'dashboard', moduleId: PLATFORM.moduleName('dashboard') },
            { route: 'settings', nav: true, name: 'settings', moduleId: PLATFORM.moduleName('settings')}
        ]);

        this.ea.publish('updateRouter');
    }

    constructor(state,ea) {
        this.state = state;
        this.ea = ea;

        this.http = axios.create({
            baseURL: 'http://localhost:3000/api/'
        });
    }

    attached() {
        feather.replace();
    }

    getDailyStats() {
        let endpoint = 'report/stats?groupBy=day';

        this.http.get(`${endpoint}`)
            .then((res) => {
                if (res.status == 200) {
                    let allCalls = res.data.sort((a, b) => moment(a.date).format('YYYYMMDD') - moment(b.date).format('YYYYMMDD'));
                    let groupCalls = allCalls.filter(c => c.callType == 0);
                    let individualCalls = allCalls.filter(c => c.callType == 1);

                    var { totalCall, diff, weekData, chartData } = this.getTotalGroupCallData(allCalls);
                    this.totalCallChartData = chartData;
                    this.totalCallCompData = {
                        title: "Total Call today",
                        totalCall: totalCall,
                        diff: diff
                    };

                    var { totalCall, diff, weekData, chartData } = this.getTotalGroupCallData(groupCalls);
                    this.groupCallData = chartData;
                    this.totalGroupCallCompData = {
                        title: "Total Group Call today",
                        totalCall: totalCall,
                        diff: diff
                    };

                    var { totalCall, diff, weekData, chartData } = this.getTotalGroupCallData(individualCalls);
                    this.individualCallData = chartData;
                    this.totalIndividualCallCompData = {
                        title: "Total Individual Call today",
                        totalCall: totalCall,
                        diff: diff
                    };

                    this.ea.publish('dailyStats:loaded');
                }
            })
            .catch((err) => console.log(err));
    }

    getMonthlyStats() {
        let endpoint = 'report/stats?groupBy=month';

        return new Promise((resolve, reject) => {
            this.http.get(`${endpoint}`)
                .then((res) => {
                    if (res.status == 200) {
                        this.state.stats = res.data;

                        this.state.siteIds = this.siteIds = [...new Set(res.data.map(s => s.deviceSiteId))];
                        this.siteIds.sort();

                        let chartData = [];
                        this.siteIds.forEach(d => {
                            chartData.push(this.constructMonthlyChart(d, res.data));
                            this.ea.publish(`${d}:monthlyStats:loaded`, chartData);
                        });

                        console.log('resolve month');
                        resolve(res.data);
                    }
                })
                .catch((err) => reject(err));
        });
    }

    getYearlyStats() {
        let endpoint = 'report/stats?groupBy=year';
        console.log('next uear');

        return new Promise((resolve, reject) => {
            this.http.get(`${endpoint}`)
                .then((res) => {
                    if (res.status == 200) {

                        let chartData = [];
                        this.siteIds.forEach(d => {
                            chartData.push(this.constructYearlyChart(d, res.data));
                            this.ea.publish(`${d}:yearlyStats:loaded`, chartData);
                        });
                        resolve(res.data);
                    }
                })
                .catch((err) => reject(err));
        });
    }

    getTotalGroupCallData(data) {
        let todayFilter = (date, isYesterday) => {
            let refDate = !isYesterday ? moment() : moment().subtract(1, "days");

            return (moment(date).format('L') == refDate.format('L'));
        };

        let totalCallToday = data
            .filter(i => todayFilter(i.date, false))
            .map(i => i.total)
            .reduce(function (x, y) { return x + y; }, 0);

        let totalCallYesterday = data
            .filter(i => todayFilter(i.date, true))
            .map(i => i.total)
            .reduce(function (x, y) { return x + y; }, 0);

        let diff = (totalCallToday - totalCallYesterday) * 100 / totalCallYesterday;

        console.log(`hari ini ${totalCallToday}`);
        console.log(totalCallYesterday);

        let lastWeek = moment().subtract(1, "weeks");
        let lastWeekData = data.filter(i => moment(i.date) > lastWeek);

        let dailyData = [];
        let lastWeekDate = [];
        for (let i = 0; i < 7; i++) {
            lastWeekDate.push(moment().subtract(i, "day").date());
        }

        for (let i = (lastWeekDate.length - 1); i >= 0; i--) {
            let _totalCallInADay = lastWeekData
                .filter(j => moment(j.date).date() == lastWeekDate[i])
                .map(i => i.total)
                .reduce(function (x, y) { return x + y; }, 0);

            dailyData.push(_totalCallInADay);
        }

        let totalCallDaily = data.filter(i => moment(i.date) > lastWeek).map(i => i.total);

        let chartData = {
            labels: ["H-6", "H-5", "H-4", "H-3", "H-2", "H-1", "H"],
            datasets: [{
                label: "Data",
                borderColor: '#4765FF',
                pointBorderWidth: 0,
                pointHoverRadius: 0,
                pointHoverBorderWidth: 0,
                pointRadius: 0,
                fill: true,
                borderWidth: 3,
                data: dailyData
            }]
        };

        return {
            totalCall: totalCallToday,
            diff: diff.toFixed(2),
            weekData: totalCallDaily,
            chartData: chartData
        }
    }

    constructMonthlyChart(deviceSiteId, data) {
        var siteId0 = data.filter(i => i.deviceSiteId == deviceSiteId);

        let getData = (type) => {
            return this.months.map(m => {
                if (siteId0.some(d => this.getMonth(d.month) == m)) {
                    let found;
                    switch (type) {
                        case CallType.IndividualCallSuccess:
                            found = siteId0.filter(d => d.callType == 1 && d.callStatus == 1)
                                .find(d => this.getMonth(d.month) == m);

                            return !!found ? found.total : 0;
                        case CallType.IndividualCallFailed:
                            found = siteId0.filter(d => d.callType == 1 && d.callStatus == 0)
                                .find(d => this.getMonth(d.month) == m)

                            return !!found ? found.total : 0;
                        case CallType.GroupCallSuccess:
                            found = siteId0.filter(d => d.callType == 0 && d.callStatus == 1)
                                .find(d => this.getMonth(d.month) == m)

                            return !!found ? found.total : 0;
                        case CallType.GroupCallFailed:
                            found = siteId0.filter(d => d.callType == 0 && d.callStatus == 0)
                                .find(d => this.getMonth(d.month) == m)

                            return !!found ? found.total : 0;
                    }
                } else {
                    return 0
                }
            })
        };

        let chartData = {
            deviceSiteId: deviceSiteId,
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets: [{
                label: 'Individual Call (Success)',
                data: getData(CallType.IndividualCallSuccess),
                backgroundColor: '#637CF9',
                borderColor: '#637CF9',
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Individual Call (Failed)',
                data: getData(CallType.IndividualCallFailed),
                backgroundColor: '#8498FA',
                borderColor: '#8498FA',
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Group Call (Success)',
                data: getData(CallType.GroupCallFailed),
                backgroundColor: '#B5C1FC',
                borderColor: '#B5C1FC',
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Group Call (Failed)',
                data: getData(CallType.GroupCallFailed),
                backgroundColor: '#E6EAFE',
                borderColor: '#E6EAFE',
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            }]
        };

        return chartData;
    }

    constructYearlyChart(deviceSiteId, data) {
        var siteId0 = data.filter(i => i.deviceSiteId == deviceSiteId);
        let years = [...new Set(data.map(x => x.year))];

        let getData = (type) => {
            return years.map(m => {
                let found;
                switch (type) {
                    case CallType.IndividualCallSuccess:
                        found = siteId0.find(d => d.callType == 1 && d.callStatus == 1);

                        return !!found ? found.total : 0;
                    case CallType.IndividualCallFailed:
                        found = siteId0.find(d => d.callType == 1 && d.callStatus == 0)

                        return !!found ? found.total : 0;
                    case CallType.GroupCallSuccess:
                        found = siteId0.find(d => d.callType == 0 && d.callStatus == 1)

                        return !!found ? found.total : 0;
                    case CallType.GroupCallFailed:
                        found = siteId0.find(d => d.callType == 0 && d.callStatus == 0)

                        return !!found ? found.total : 0;
                }
            });
        };

        let chartData = {
            deviceSiteId: deviceSiteId,
            labels: years,
            datasets: [{
                label: 'Individual Call (Success)',
                data: getData(CallType.IndividualCallSuccess),
                backgroundColor: '#637CF9',
                borderColor: '#637CF9',
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Individual Call (Failed)',
                data: getData(CallType.IndividualCallFailed),
                backgroundColor: '#8498FA',
                borderColor: '#8498FA',
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Group Call (Success)',
                data: getData(CallType.GroupCallFailed),
                backgroundColor: '#B5C1FC',
                borderColor: '#B5C1FC',
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            },
            {
                label: 'Group Call (Failed)',
                data: getData(CallType.GroupCallFailed),
                backgroundColor: '#E6EAFE',
                borderColor: '#E6EAFE',
                borderWidth: 3,
                radius: 0,
                pointStyle: 'line'
            }]
        };

        return chartData;
    }

    getMonth(id) {
        return this.months[id -1];
    }
}
