﻿export const CallType = {
    GroupCallSuccess: 0,
    GroupCallFailed: 1,
    IndividualCallSuccess: 2,
    IndividualCallFailed: 3
}