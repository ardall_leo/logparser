﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace LogParserWeb
{
    public class Program
    {
        static ILogger logger;
        static IConfigurationRoot configuration;
        static string appUrl;
        public static void Main(string[] args)
        {
            logger = new LoggerConfiguration()
            .WriteTo.Console()
            .CreateLogger();

            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            configuration = builder.Build();
            appUrl = $"http://localhost:{configuration.GetSection("Port").Value}";
            try
            {
                logger.Information("Intializing...");

                var build = CreateHostBuilder(args).Build();

                logger.Information($"Starting at {appUrl}");

                build.Run();
            }
            catch (Exception ex)
            {
                logger.Fatal(ex, "Host terminated unexpectedly!");
            }
            finally
            {
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>().UseUrls(appUrl);
                });
    }
}
